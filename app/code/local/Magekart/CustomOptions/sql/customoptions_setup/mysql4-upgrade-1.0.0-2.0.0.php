<?php
/* @var $installer Magekart_CustomOptions_Model_Mysql4_Setup */
$installer = $this;
$installer->startSetup();
$installer->run("ALTER TABLE `{$installer->getTable('catalog/product_option')}` CHANGE `in_group_id` `in_group_id` INT UNSIGNED NOT NULL DEFAULT '0'");
$installer->run("ALTER TABLE `{$installer->getTable('catalog/product_option_type_value')}` CHANGE `in_group_id` `in_group_id` INT UNSIGNED NOT NULL DEFAULT '0'");

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'is_dependent')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option'),
        'is_dependent',
        "tinyint(1) NOT NULL DEFAULT '0'"
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option_type_value'), 'dependent_ids')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option_type_value'),
        'dependent_ids',
        "varchar(255) NOT NULL DEFAULT ''"
    );
}

$installer->run("UPDATE `{$installer->getTable('catalog/product_option')}` AS cpo, `{$installer->getTable('customoptions/relation')}` AS cor 
    SET cpo.`in_group_id`=(cor.`group_id` * 65535) + cpo.`in_group_id`
    WHERE cpo.`option_id`=cor.`option_id` AND cpo.`in_group_id`>0 AND cpo.`in_group_id` < 65536 AND cor.`group_id`>0 AND cor.`group_id` IS NOT NULL");

$installer->run("UPDATE `{$installer->getTable('catalog/product_option_type_value')}` AS cpotv, `{$installer->getTable('customoptions/relation')}` AS cor 
    SET cpotv.`in_group_id`=(cor.`group_id` * 65535) + cpotv.`in_group_id`
    WHERE cpotv.`option_id`=cor.`option_id` AND cpotv.`in_group_id`>0 AND cpotv.`in_group_id` < 65536 AND cor.`group_id`>0 AND cor.`group_id` IS NOT NULL");
	
$installer->run("ALTER TABLE `{$installer->getTable('catalog/product_option_type_value')}` CHANGE `dependent_ids` `dependent_ids` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ''");

$installer->run("UPDATE IGNORE `{$this->getTable('core_config_data')}` SET `path` = REPLACE(`path`,'magekart_sales/','magekart_catalog/') WHERE `path` LIKE 'magekart_sales/customoptions/%'");

if ($installer->getConnection()->tableColumnExists($installer->getTable('customoptions/group'), 'store_id')) {
    $installer->run("ALTER TABLE `{$installer->getTable('customoptions/group')}` DROP `store_id`;");
}

$installer->run("-- DROP TABLE IF EXISTS `{$installer->getTable('customoptions/group_store')}`;
CREATE TABLE IF NOT EXISTS `{$installer->getTable('customoptions/group_store')}` (
  `group_store_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,  
  `hash_options` longtext NOT NULL,
  PRIMARY KEY (`group_store_id`),
  UNIQUE KEY `UNQ_CUSTOM_OPTIONS_GROUP_STORE` (`group_id`,`store_id`),
  CONSTRAINT `FK_MAGEKART_CUSTOM_OPTIONS_GROUP_STORE` FOREIGN KEY (`group_id`) REFERENCES `{$installer->getTable('customoptions/group')}` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");


if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'div_class')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option'),
        'div_class',
        "varchar(64) NOT NULL default ''"
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option_type_value'), 'weight')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option_type_value'),
        'weight',
        "DECIMAL( 12, 4 ) NOT NULL DEFAULT '0'"
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product'), 'absolute_weight')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product'),
        'absolute_price',
        "TINYINT (1) NOT NULL DEFAULT 0"
    );
    
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product'),
        'absolute_weight',
        "TINYINT (1) NOT NULL DEFAULT 0"
    );
}


if (!$installer->getConnection()->tableColumnExists($installer->getTable('customoptions/group'), 'absolute_weight')) {    
    $installer->getConnection()->addColumn(
        $installer->getTable('customoptions/group'),
        'absolute_price',
        "TINYINT (1) NOT NULL DEFAULT 0"
    );
    
    $installer->getConnection()->addColumn(
        $installer->getTable('customoptions/group'),
        'absolute_weight',
        "TINYINT (1) NOT NULL DEFAULT 0"
    );
}

$installer->run("-- DROP TABLE IF EXISTS {$installer->getTable('customoptions/option_default')};
CREATE TABLE IF NOT EXISTS `{$installer->getTable('customoptions/option_default')}` (
  `option_default_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option_id` int(10) unsigned NOT NULL DEFAULT '0',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `default_text` text NOT NULL,
  PRIMARY KEY (`option_default_id`),
  UNIQUE KEY `option_id+store_id` (`option_id`,`store_id`),
  KEY `store_id` (`store_id`),
  CONSTRAINT `FK_MAGEKART_CUSTOM_OPTIONS_DEFAULT_OPTION` FOREIGN KEY (`option_id`) REFERENCES `{$installer->getTable('catalog/product_option')}` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MAGEKART_CUSTOM_OPTIONS_DEFAULT_STORE` FOREIGN KEY (`store_id`) REFERENCES `{$installer->getTable('core/store')}` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

ALTER TABLE `{$installer->getTable('customoptions/option_description')}` CHANGE `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '';
DELETE FROM `{$installer->getTable('customoptions/option_description')}` WHERE `description` = '';");

$installer->endSetup();