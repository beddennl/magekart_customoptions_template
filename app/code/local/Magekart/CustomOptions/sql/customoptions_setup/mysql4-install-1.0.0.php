<?php
/* @var $installer Magekart_CustomOptions_Model_Mysql4_Setup */
$installer = $this;

$installer->startSetup();

$installer->run("
-- DROP TABLE IF EXISTS {$installer->getTable('customoptions/group')};
CREATE TABLE IF NOT EXISTS {$installer->getTable('customoptions/group')} (
  `group_id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `is_active` tinyint(1) NOT NULL,
  `store_id` smallint(5) unsigned default NULL,
  `hash_options` longtext NOT NULL,
   PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- DROP TABLE IF EXISTS {$installer->getTable('customoptions/relation')};
CREATE TABLE IF NOT EXISTS {$installer->getTable('customoptions/relation')} (
  `id` int(10) unsigned NOT NULL auto_increment,
  `group_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `option_id` int(10) unsigned NOT NULL,
   PRIMARY KEY (`id`),
   UNIQUE KEY `UNQ_MAGEKART_CUSTOM_RELATION` (`group_id`,`option_id`,`product_id`),
   CONSTRAINT `FK_MAGEKART_CUSTOM_OPTIONS_INDEX_PRODUCT_ENTITY` FOREIGN KEY (`product_id`) REFERENCES `{$installer->getTable('catalog/product')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
   CONSTRAINT `FK_MAGEKART_CUSTOM_OPTIONS_INDEX_GROUP_RELATION` FOREIGN KEY (`group_id`) REFERENCES `{$installer->getTable('customoptions/group')}` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- DROP TABLE IF EXISTS {$installer->getTable('customoptions/option_description')};
CREATE TABLE IF NOT EXISTS {$installer->getTable('customoptions/option_description')} (
  `option_description_id` int(10) unsigned NOT NULL auto_increment,
  `option_id` int(10) unsigned NOT NULL default '0',
  `store_id` smallint(5) unsigned NOT NULL default '0',
  `description` text,
  PRIMARY KEY  (`option_description_id`),
  KEY `MAGEKART_CUSTOM_OPTIONS_DESCRIPTION_OPTION` (`option_id`),
  KEY `MAGEKART_CUSTOM_OPTIONS_DESCRIPTION_STORE` (`store_id`),
  CONSTRAINT `FK_MAGEKART_CUSTOM_OPTIONS_DESCRIPTION_OPTION` FOREIGN KEY (`option_id`) REFERENCES `{$installer->getTable('catalog/product_option')}` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MAGEKART_CUSTOM_OPTIONS_DESCRIPTION_STORE` FOREIGN KEY (`store_id`) REFERENCES `{$installer->getTable('core/store')}` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'customoptions_status')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option'),
        'customoptions_status',
        'tinyint(1) NOT NULL default 0'
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option_type_value'), 'customoptions_qty')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option_type_value'),
        'customoptions_qty',
        "varchar(10) NOT NULL default ''"
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'customoptions_is_onetime')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option'),
        'customoptions_is_onetime',
        'TINYINT (1) NOT NULL DEFAULT 0'
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'image_path')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option'),
        'image_path',
        "varchar (255) default ''"
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option_type_value'), 'default')) {
    $installer->getConnection()->addColumn(
            $installer->getTable('catalog/product_option_type_value'),
            'default',
            "tinyint(1) NOT NULL DEFAULT '0'"
    );
}    

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'customer_groups')) {
    $installer->getConnection()->addColumn(
            $installer->getTable('catalog/product_option'),
            'customer_groups',
            "varchar (255) default ''"
    );
}
if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'qnty_input')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option'),
        'qnty_input',
        "tinyint(1) NOT NULL DEFAULT '0'"
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'in_group_id')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option'),
        'in_group_id',
        "SMALLINT UNSIGNED NOT NULL DEFAULT '0'"
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option_type_value'), 'in_group_id')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('catalog/product_option_type_value'),
        'in_group_id',
        "SMALLINT UNSIGNED NOT NULL DEFAULT '0'"
    );
}    


if ($installer->getConnection()->tableColumnExists($installer->getTable('catalog/product_option'), 'customoptions_status')) {
    $installer->getConnection()->dropColumn(
        $installer->getTable('catalog/product_option'),
        'customoptions_status'
    );
}

$installer->getConnection()->addKey($installer->getTable('customoptions/relation'), 'option_id', 'option_id'); 

$installer->endSetup();
