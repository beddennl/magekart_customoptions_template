<?php
if (version_compare(Mage::helper('customoptions')->getMagetoVersion(), '1.8.0', '<')) {
    class Magekart_CustomOptions_Model_Importexport_Export_Entity_Product extends Magekart_CustomOptions_Model_Importexport_Export_Entity_Product_M1700 {}
} else {
    class Magekart_CustomOptions_Model_Importexport_Export_Entity_Product extends Magekart_CustomOptions_Model_Importexport_Export_Entity_Product_M1800 {}
}
